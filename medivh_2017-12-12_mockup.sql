# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Hôte: 127.0.0.1 (MySQL 5.7.20)
# Base de données: medivh
# Temps de génération: 2017-12-12 13:49:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table channel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `channel`;

CREATE TABLE `channel` (
  `channel_id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `channel_name` varchar(25) NOT NULL DEFAULT '',
  `channel_type` varchar(25) DEFAULT 'text',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;

INSERT INTO `channel` (`channel_id`, `channel_name`, `channel_type`)
VALUES
	(1,'General','text'),
	(2,'UX','text'),
	(3,'M1DEVB','text'),
	(4,'M1DEVA','text'),
	(5,'M2DEV','text');

/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table friendship
# ------------------------------------------------------------

DROP TABLE IF EXISTS `friendship`;

CREATE TABLE `friendship` (
  `friendship_id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `friendship_id_user_asker` int(30) NOT NULL,
  `friendship_id_user_target` int(30) NOT NULL,
  `friendship_status` varchar(25) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`friendship_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `friendship` WRITE;
/*!40000 ALTER TABLE `friendship` DISABLE KEYS */;

INSERT INTO `friendship` (`friendship_id`, `friendship_id_user_asker`, `friendship_id_user_target`, `friendship_status`)
VALUES
	(1,1,2,'pending'),
	(2,1,3,'accepted'),
	(3,2,3,'pending');

/*!40000 ALTER TABLE `friendship` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table message
# ------------------------------------------------------------

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `message_id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `message_content` longtext NOT NULL,
  `message_datetime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `message_author` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;

INSERT INTO `message` (`message_id`, `message_content`, `message_datetime`, `message_author`)
VALUES
	(1,'Salut, ça va','2017-12-12 14:36:44','Florian'),
	(2,'J\'aime beaucoup l\'intégration','2017-12-12 14:37:32','Bastien'),
	(3,'Le prof de PHP est vraiment cool','2017-12-12 14:40:32','Bastien'),
	(4,'C\'est bientôt Noël c\'est cool :)','2017-12-12 14:43:32','Florian');

/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(30) unsigned NOT NULL AUTO_INCREMENT,
  `user_pseudo` varchar(25) NOT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_role` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`user_id`, `user_pseudo`, `user_password`, `user_role`)
VALUES
	(1,'Florian','florian','user'),
	(2,'BastienD','bastiend','admin'),
	(3,'Vincent','vincent','user');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
