<?php

require_once('vendor/autoload.php');

define('ROOT_PATH', __DIR__);

use Onyxia\Component\Routing;

$routingService = new Routing();

session_start();

// ROUTING
if (empty($_GET['action'])) {
    $action = 'home';
} else {
    $action = $_GET['action'];
}

// CONTROLLER
$controller = $routingService->getRootAction($action);

$controller_path = 'controllers/'.$controller.'.php';

$view = $action;

if (is_file($controller_path)) {
    include($controller_path);
} else {
    $routingService->generate404('Error 404, controller not found');
}

// VIEW
$view_path = 'resources/views/'.$view.'.php';

if (isset($displayJSON) && is_file($view_path)) {
    include($view_path);
}
elseif (is_file($view_path)) {
    include('resources/views/layout.php');
} else {
    $routingService->generate404('Error 404, view not found');
}
