export let mock = {
    "room_1": {
        "name": "General",
        "message": [
            {
                "message_id": "1",
                "message_content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                "message_datetime": "2017-12-12 14:36:44",
                "message_author": "Florian"
            },
            {
                "message_id": "2",
                "message_content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                "message_datetime": "2017-12-12 14:36:44",
                "message_author": "Bastien"
            }]
    },
    "room_2": {
        "name": "UX",
        "message": [
            {
                "message_id": "1",
                "message_content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                "message_datetime": "2017-12-12 14:36:44",
                "message_author": "Florian"
            },
            {
                "message_id": "2",
                "message_content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                "message_datetime": "2017-12-12 14:36:44",
                "message_author": "Bastien"
            }]
    }
    ,
    "room_3": {
        "name": "M1DEV",
        "message": [
            {
                "message_id": "1",
                "message_content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                "message_datetime": "2017-12-12 14:36:44",
                "message_author": "Florian"
            },
            {
                "message_id": "2",
                "message_content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                "message_datetime": "2017-12-12 14:36:44",
                "message_author": "Bastien"
            }]
    }
};