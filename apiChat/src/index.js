import {mock} from "./mock";
import {adapter} from "./services/adapter.service";
import {RoomService} from "./services/room.service";

const mod = process.env.MOD;
// We create an http server using Express
const express = require('express');
const app = express();
const http = require('http').Server(app);
// We create a socket.io instance using the package in node module
export const io = require('socket.io')(http);
// We create a Request instance using the package in node module
export const request = require("request");

// We create a Promise that will get every data we need from our PHP system thought HTTP request
let getData = new Promise((resolve, reject) => {
    request("http://localhost/ECV/Projet/Cenarius/?action=api", function (error, response, body) {
        if(mod === 'mock'){
            resolve(mock);
        }else{
            resolve(JSON.parse(body)); 
        }
        reject(error);
    });
});

// When we get the data from Onyxia we start emitting and listening to our client
getData.then(
    function (val) {
        // We build a map with the JSON we just get
        let dataTreat = new adapter(val);
        dataTreat.mapBuilder();
        // We emit and listen chat data
        let dataHandler = new RoomService(dataTreat.roomMap);
        dataHandler.emitAndListenStart();
        // We start our server
        http.listen(3000, function () {
            console.log('Server started on port 3000')
        })
    }
).catch(() => {
    console.log("error")
});

