import {io, request} from "../index";

/**
 *
 * This class take a map of room and associated message as parameter then emit events for each room their is every time a client connect.
 * It also listen to client in order to update the message contain in the map and send an HTTP Request to the Onyxia System
 *
 * **/

export class RoomService {

    // We initialize the class with a map we get trough parameter
    constructor(roomMap) {
        this.roomMap = roomMap;
    }

    // This method will emit and listen with the map it get from initialisation
    emitAndListenStart() {

        // We create a Promise that will make sure that our map is fully initialized
        let roomPromise = new Promise((resolve) => {
            resolve(this.roomMap);
        });

        io.on('connection', function (socket) {
            console.log('a user connected');
            // When our map is ok we go trough it then we emit/listen event accordingly
            roomPromise.then(function (map) {
                for (let room of map) {
                    socket.emit(room[0], room[1]);
                    socket.on(room[0], (data) => {
                        map.set(map.get(room[1].message.push(data)));
                        socket.emit(room[0], room[1]);
                        // We send to the Onyxia System the data it need to store the message
                        request({
                            uri: "http://localhost/ECV/Projet/Cenarius/?action=apiAdd",
                            method: "POST",
                            form: {
                                room_id: room[0],
                                message: data
                            }
                        }, function (error) {
                            if(error === null){
                                console.log('data sent');
                            }
                        });
                    });
                }
            });
        });
    }
}
