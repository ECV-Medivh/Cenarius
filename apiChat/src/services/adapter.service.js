/**
 *
 * That class will create a map from a JSON it get
 *
 * */

export class adapter {
    constructor(data) {
        this.data = data;
        this.roomMap = new Map();
    }
    mapBuilder(){
        let data = this.data;
        for(let node in data){
            this.roomMap.set(node,data[node]);
        }
    }
}