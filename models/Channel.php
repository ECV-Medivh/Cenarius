<?php

use Onyxia\Component\Data;

Class Channel extends Data
{
    public function __construct($channel_id = null)
    {
        parent::__construct();
        $this->pk = 'channel_id';
        $this->table_name = 'channel';
        $this->fields = [
            'channel_id',
            'channel_name',
            'channel_type'
        ];
        if ($channel_id != null) {
            $this->channel_id = $channel_id;
            $this->hydrate();
        }
    }
}