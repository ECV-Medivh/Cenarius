<?php

use Onyxia\Component\Data;

Class User extends Data
{
    public function __construct($user_id = null)
    {
        parent::__construct();
        $this->pk = 'user_id';
        $this->table_name = 'user';
        $this->fields = [
            'user_id',
            'user_pseudo',
            'user_password',
            'user_role'
        ];
        if ($user_id != null) {
            $this->user_id = $user_id;
            $this->hydrate();
        }
    }
}