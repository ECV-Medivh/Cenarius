<?php

use Onyxia\Component\Data;

Class Message extends Data
{
    public function __construct($message_id = null)
    {
        parent::__construct();
        $this->pk = 'message_id';
        $this->table_name = 'message';
        $this->fields = [
            'message_id',
            'message_content',
            'message_datetime',
            'message_author'
        ];
        if ($message_id != null) {
            $this->message_id = $message_id;
            $this->hydrate();
        }
    }
}