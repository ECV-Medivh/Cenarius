-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 19 Décembre 2017 à 13:16
-- Version du serveur :  5.7.20-0ubuntu0.16.04.1
-- Version de PHP :  7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `medivh`
--

-- --------------------------------------------------------

--
-- Structure de la table `channel`
--

CREATE TABLE `channel` (
  `channel_id` int(30) UNSIGNED NOT NULL,
  `channel_name` varchar(25) NOT NULL DEFAULT '',
  `channel_type` varchar(25) DEFAULT 'text'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `channel`
--

INSERT INTO `channel` (`channel_id`, `channel_name`, `channel_type`) VALUES
(1, 'General', 'text'),
(2, 'Blabla', 'text');

-- --------------------------------------------------------

--
-- Structure de la table `friendship`
--

CREATE TABLE `friendship` (
  `friendship_id` int(30) UNSIGNED NOT NULL,
  `friendship_id_user_asker` int(30) NOT NULL,
  `friendship_id_user_target` int(30) NOT NULL,
  `friendship_status` varchar(25) NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `message_id` int(30) UNSIGNED NOT NULL,
  `channel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_content` longtext NOT NULL,
  `message_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_author` varchar(25) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`message_id`, `channel_id`, `user_id`, `message_content`, `message_datetime`, `message_author`) VALUES
(1, 1, 1, 'Bonjour', '2017-12-18 13:02:57', 'bastien'),
(2, 1, 2, 'Bonsoir', '2017-12-18 13:02:57', 'Florian'),
(3, 1, 2, 'bonjour', '2017-12-18 20:51:49', 'bleuh'),
(4, 2, 1, '', '2017-12-18 20:58:06', 'aa'),
(5, 1, 7, 'salut', '2017-12-19 13:02:00', '');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `user_id` int(30) UNSIGNED NOT NULL,
  `user_pseudo` varchar(25) NOT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_role` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`user_id`, `user_pseudo`, `user_password`, `user_role`) VALUES
(1, 'bleuh', '1996neitsab', NULL),
(2, 'boulou', 'loulou', 'NULL'),
(3, 'dde', 'dede', NULL),
(4, 'etete', 'etetet', NULL),
(5, 'errere', 'erere', NULL),
(6, 'eaez', 'aeazeza', NULL),
(7, 'aa', 'aa', NULL),
(8, 'aze', 'aze', NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `channel`
--
ALTER TABLE `channel`
  ADD PRIMARY KEY (`channel_id`);

--
-- Index pour la table `friendship`
--
ALTER TABLE `friendship`
  ADD PRIMARY KEY (`friendship_id`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `pseudo` (`user_pseudo`) USING BTREE;

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `channel`
--
ALTER TABLE `channel`
  MODIFY `channel_id` int(30) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `friendship`
--
ALTER TABLE `friendship`
  MODIFY `friendship_id` int(30) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(30) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(30) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
