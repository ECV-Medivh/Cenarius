<?php

use Onyxia\Component\Language;
use Onyxia\Component\Query;
use Onyxia\Component\Routing;
require './models/User.php';

$languageService = new Language();
$queryService = new Query();
$routingService = new Routing();

$pseudo = $languageService->getTranslationFor('pseudo');
$password = $languageService->getTranslationFor('password');
$login = $languageService->getTranslationFor('login');
$signup = $languageService->getTranslationFor('signup');
//$forgotPassword= $languageService->getTranslationFor('forgot password');

if ($action == 'login') {
    if (empty($_POST['login-pseudo']) || empty($_POST['login-password'])) {
        $message = $languageService->getTranslationFor('form-missing');
    }
    elseif ($user = $queryService->userExist($_POST['login-pseudo'], $_POST['login-password'])) {
        $_SESSION['user'] = serialize(new User($user['user_id']));
        $routingService->redirect('home');
    }
    else {
        $message = $languageService->getTranslationFor('form-not-exist');
    }
} elseif ($action == 'register') {
    if (empty($_POST['registration-pseudo']) || empty($_POST['registration-pseudo'])) {
        $message = $languageService->getTranslationFor('form-missing');
    }
    $message = $queryService->userRegistration($_POST['registration-pseudo'], $_POST['registration-password']);
    $routingService->redirect('home_login');
} elseif ($action == 'home_login') {
    $view = "login";
} elseif ($action == "logout") {
    session_destroy();
    $routingService->redirect('home');
}
