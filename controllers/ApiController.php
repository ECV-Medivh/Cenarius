<?php

use Onyxia\Component\Language;
use Onyxia\Component\Query;
use Onyxia\Component\Routing;
require './models/User.php';

$languageService = new Language();
$queryService = new Query();
$routingService = new Routing();

if ($action == 'api')
{
    $arrayToJson = $queryService->getChannels();
    $json = json_encode($arrayToJson);
    $displayJSON = true;
    $view = 'json';
}
elseif($action == 'apiAdd') {
    // if (empty($_POST['dataSend'])) {
    //     $routingService->displayError('no message send');
    // }
    $room_id = $_POST['room_id'];
    $message = $_POST['message'];
    // $server_id = 'room_1';
    // $message = $_POST['chat-add'];
    $queryService->addMessage($room_id, $message);
    $displayJSON = true;
    $view = 'json';
}