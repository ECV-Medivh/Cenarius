<?php

use Onyxia\Component\Language;
use Onyxia\Component\Query;
use Onyxia\Component\Routing;

$languageService = new Language();
$queryService = new Query();
$routingService = new Routing();
require './models/User.php';

if ($action == 'createRoom')
{
	$roomName = $languageService->getTranslationFor('room name');
    if (empty($_POST['room-name'])) {
        $view = "/components/createRoom";
    }
    else{
        $room_name = $_POST['room-name'];
        $user_id = unserialize($_SESSION['user'])->user_id;
        $queryService->createRoomForUser($room_name, $user_id);
        $routingService->redirect('home');
    }
}
