// --- LOGIN
// Function to change the form type
function changeTab(index) {
    var tab = document.getElementsByClassName('tab-target');
    var loginForm = document.getElementById('login');
    var signupForm = document.getElementById('signup');
    // if the choice is "Login"
    if (index === 0) {
        tab[0].parentElement.className = 'tab active';
        tab[1].parentElement.className = 'tab';
        loginForm.style["display"] = "block";
        signupForm.style["display"] = "none";
        // if the choice is "Sign up"
    } else if (index === 1) {
        tab[0].parentElement.className = 'tab';
        tab[1].parentElement.className = 'tab active';
        loginForm.style["display"] = "none";
        signupForm.style["display"] = "block";
    }
}

// --- CHAT

var socket = io('localhost:3000');

var selectedRoom = roomArray[0];

var messageData;
var lengthTab;
var messageId;

var roomList = []

for (var room of roomArray) {
  roomList.push(buildRoom(room));
}

function buildRoom(room) {
    var roomView = new Vue({
        el: '#' + 'room_' +room,
        data: {
            selectedRoom: null,
            message: []
        },
        computed: {}
    }
    viewDefinition.computed[room + '_visibility'] = function(){
      return this.selectedRoom === room;

    }
    var roomView = new Vue(viewDefinition);
    socket.on(room,(function (data) {
        //console.log(data);
        messageData = data;
        lengthTab = messageData['message'].length;
        messageId = messageData['message'][lengthTab - 1]['message_id'];
        roomView.message = messageData['message'];
    }));
    return roomView
}
function roomSelector(room_id){
    selectedRoom = room_id;
    for (r of roomList) {
      r.selectedRoom = room_id
    }
    //console.log(room_id);
}
function handle(e) {
    var textBox = document.getElementById('textBox');
    var userPseudo = textBox.dataset.pseudo;
    var room = textBox.dataset.room;
    if (e.keyCode === 13) {
        messageId = parseInt(messageId) + 1;
        e.preventDefault();
        var message = {
            message_id: messageId,
            message_content: textBox.value,
            message_datetime: getDate(),
            message_author: userPseudo
        };
        //console.log(selectedRoom);
        socket.emit(selectedRoom, message);
        textBox.value = '';
    }
}

function getDate() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var seconde = now.getSeconds();

    return result = year + '-' + day + '-' + month + ' ' + hour + ':' + minute + ':' + seconde;
}
