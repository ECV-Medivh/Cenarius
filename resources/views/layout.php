<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title><?php echo ucfirst($action); ?></title>
  <link rel="stylesheet" type="text/css" href="./public/assets/css/style.min.css">
</head>
<body>
	<?php include($view_path); ?>
  <script>
    var json = <?php echo json_encode($roomArrayId) ?>;
    var roomArray = [];
    for( let node in json)
    {
      roomArray.push(node)
    }
  </script>
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
  <script src="./public/assets/js/scripts.js" type="text/javascript"></script>
</body>
</html>
