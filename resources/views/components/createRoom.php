<div class="createRoom-container">
  <form class="createRoom-form" action="?action=createRoom" method="post">
    <label class="room-label" for="room-name"><?= $roomName ?></label>
    <input class="form-input" type="text" name="room-name" value="">
  </form>
</div>
