<div class="room-container">
   <?php if (!empty($roomArray)) {
      foreach ($roomArray as $room) {
          ?>
   <div class="room"><a class="room-link roomList" href="#" onclick="roomSelector('<?php echo $room ?>')"><?= $room ?></a></div>
   <?php
      }
      ?>
   <div class="room"><a class="room-link" href="?action=createRoom"><?= $addRoom ?></a></div>
</div>
<div class="chat-container">
   <?php foreach ($roomArray as $room) { ?>
   <div class="messages-container" id="<?php echo( 'room_' . $room) ?>">
      <li class="message" v-for="msg in message">
         {{ msg.message_content }}
      </li>
   </div>
   <?php } }?>
</div>
