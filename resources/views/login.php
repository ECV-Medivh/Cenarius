<div class="login-container">

  <div class="form">

      <ul class="tab-group">
          <li class="tab active">
              <a class="tab-target" href="#login" onclick="changeTab(0)">
                  <?= $login ?>
              </a>
          </li>
          <li class="tab">
              <a class="tab-target" href="#signup" onclick="changeTab(1)"><?= $signup ?></a>
          </li>
      </ul>

      <div class="tab-content">
          <div id="login">

              <form action="?action=login" method="post">

                  <div class="field">
                      <label class="form-label"><?= $pseudo ?><span class="req">*</span></label>
                      <input class="form-input" type="text" required autocomplete="off" name="login-pseudo" />
                  </div>

                  <div class="field">
                      <label class="form-label"><?= $password ?><span class="req">*</span></label>
                      <input class="form-input" type="password" required autocomplete="off" name="login-password" />
                  </div>
                  <?php if (!empty($message)) { ?>
                      <div>
                          <?= $message ?>
                      </div>
                  <?php } ?>
                      <!-- <p class="forgot"><a href="#">Forgot Password?</a></p> -->
                      <button class="primary-button" /><?= $login ?></button>
              </form>
          </div>
          <div id="signup">

              <form action="?action=register" method="post">
                  <div class="field">
                      <label class="form-label"><?= $pseudo ?><span class="req">*</span></label>
                      <input class="form-input" type="text" required autocomplete="off" name="registration-pseudo" />
                  </div>

                  <div class="field">
                      <label class="form-label"><?= $password ?><span class="req">*</span></label>
                      <input class="form-input" type="password" required autocomplete="off" name="registration-password" />
                  </div>

                  <button type="submit" class="primary-button button-block" /><?= $signup ?></button>

              </form>

          </div>
      </div>
  </div>
</div>
