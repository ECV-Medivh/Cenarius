var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

// --- CSS

gulp.task('css', function () {
    return gulp.src('resources/assets/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/assets/css'))
        .pipe(cssnano())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/assets/css'))
});

// --- JS

gulp.task('js', function () {
    gulp.src('resources/assets/js/scripts.js')
        .pipe(gulp.dest('public/assets/js'))
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })

        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/assets/js'))
});

// --- WATCH

// gulp.task('all', ['css'],['js'], function () {
//     gulp.watch("resources/assets/scss/**/*.scss", ['css']);
//     gulp.watch("resources/assets/js/*.js", ['js']);
// });
