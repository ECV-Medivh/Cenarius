# Cenarius

## Setup the project

1. Configure your database connection in /config/parameters.yml
2. Change the application language if you wish to do so (in English by default). Configuration file: /config/language. yml

## Dependency Installation

```
composer install
```

```
npm install
```
